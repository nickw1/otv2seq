import psycopg2
import dotenv
import math
import sys
import os
import shutil
#from PIL import Image
import io

def get_panos(conn, start=None, end=None):
    cur = conn.cursor()
    if start is not None and end is not None:
        cur.execute("SELECT id, ST_X(the_geom) AS lon, ST_Y(the_geom) AS lat FROM panoramas WHERE id>=%s AND id<=%s ORDER BY id", (start, end))
    else:
        cur.execute("SELECT id, ST_X(the_geom) AS lon, ST_Y(the_geom) AS lat FROM panoramas ORDER BY id")
    return cur.fetchall()
    

#def add_latlon_to_panos(results):
#	for row in results:	
#		imWithExif = Image.open(f"{os.environ.get('PANODIR'}/{row[0]}.jpg")
#		imWithExif.info['exif'].GPSLongitude = row[1]
#		imWithExif.info['exif'].GPSLatitude = row[2]
#		buffer = io.BytesIO()
	 


def get_sequences(results, limit):
    sequences = [[]]
    seqno = 1
    for i in range(1,len(results)):
        if os.path.exists(f"{os.environ.get('PANODIR')}/{results[i-1][0]}.jpg"):
            sequences[seqno-1].append(results[i-1][0])
            dist = haversine_dist(results[i-1][1], results[i-1][2], results[i][1], results[i][2])
            if dist > limit:
                seqno += 1
                sequences.append([])
        else:
            print(f"Warning: pano #{results[i-1][0]} is in DB, but no file found")
    return sequences

def create_sequence_dirs(sequences):
    for seqid in range(1, len(sequences)+1):
        print(f"Doing sequence #{seqid}")
        seqdir = f"{os.environ.get('SEQDIR')}/{seqid}"
        try:
            os.mkdir(seqdir)
        except FileExistsError as e:
            print(f"Warning, directory for sequence #{seqid} already exists")

        for panoid in sequences[seqid-1]:
            shutil.copy(f"{os.environ.get('PANODIR')}/{panoid}.jpg", seqdir)

def haversine_dist(lon1, lat1, lon2, lat2):
    R = 6371000
    dlon = (lon2-lon1)*(math.pi / 180)
    dlat = (lat2-lat1)*(math.pi / 180)
    slat = math.sin(dlat/2)
    slon = math.sin(dlon/2)
    a = slat**2 + math.cos(lat1*(math.pi/180))*math.cos(lat2*(math.pi/180))*(slon**2)
    c = 2 * math.asin(min(1,math.sqrt(a)))
    return R*c

def main():
    if len (sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} distancelimit startpano endpano")
    else:
        start = None
        end = None
        limit = int(sys.argv[1])
        if len (sys.argv) >= 4:
            start = sys.argv[2]
            end = sys.argv[3]

        dotenv.load_dotenv()

        conn = psycopg2.connect(f"dbname={os.environ.get('DB_NAME')} user={os.environ.get('DB_USER')}")
        results = get_panos(conn, start, end)
        conn.close()

        sequences = get_sequences(results, limit)
        print(f"Created {len(sequences)} sequences.")
        create_sequence_dirs(sequences)

if __name__ == "__main__":
    main()
