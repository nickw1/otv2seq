# otv2seq

Creates sequences, ready for [GeoVisio](https://gitlab.com/geovisio), from an [OpenTrailView](https://opentrailview.org) database and panorama collection.

## Install

Requires `psycopg2` and `python-dotenv`.

```
pip3 install -r requirements.txt
```

## Configuration

`otv2seq` uses a `.env` file to specify environment variables. These variables must be specified:

- `DB_USER` : the database user;
- `DB_NAME` : the database;
- `PANODIR` : the directory containing the OpenTrailView panoramas;
- `SEQDIR` : the directory which will include the sequence directories.

## Usage

`python3 otv2seq.py distance [startPanoId endPanoId]`

where `startPanoId` is the first panorama ID to process, `endPanoId` is the last, and `distance` is the maximum distance between panoramas allowed in a sequence. So if the distance between the two panoramas is greater than this, the sequence will break.

If `startPanoId` and `endPanoId` are not specified, all panoramas in the database will be processed.

Panoramas are processed in ID order, so if a sequence has non-consecutive IDs, it will not be recognised as a sequence.

## Result

Output is a series of directories, named using numerical sequence IDs, containing each sequence.

